// Package gargsxd provides an argument type and parser similar to rargsxd
// (codeberg.org/BubbyRoosh/rargsxd) to use as an alternative to the flag
// library.
package gargsxd

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Struct Arg is the base argument type that will hold the information for each
// argument along with its Value.
type Arg struct {
	// Help contains the message that will be printed for the argument when
	// PrintHelp is called.
	Help string
	// Short is the rune that is used when the argument is passed with `-`.
	Short rune
	// Long is the string that is used when the argument is passed with `--`.
	Long string
	// Value is the "generic" type for the value of the argument. When proper
	// generics are fully implemented and official/stable, interface{} will be
	// replaced.
	Value interface{}
}

// Bool is a helper function to return an argument with a bool value.
func Bool(short rune, long string, def bool, help string) Arg {
	return Arg{
		Help:  help,
		Short: short,
		Long:  long,
		Value: def,
	}
}

// Int is a helper function to return an argument with an int value.
func Int(short rune, long string, def int, help string) Arg {
	return Arg{
		Help:  help,
		Short: short,
		Long:  long,
		Value: def,
	}
}

// String is a helper function to return an argument with a string value.
func String(short rune, long string, help string) Arg {
	return Arg{
		Help:  help,
		Short: short,
		Long:  long,
		Value: "",
	}
}

// Returns os.Args[0] for setting ArgParser.Name
func Argv0() string {
	return os.Args[0]
}

// Returns a default Usage string for setting ArgParser.Usage
func Usage() string {
	return "{name} [flags] [options]"
}

// Struct ArgParser is the main parser holding information about the program and
// its arguments.
type ArgParser struct {
	// Name is the name of the program (automatically set by passing Argv0()).
	Name string
	// Author is the name of the program's author(s).
	Author string
	// Version is the stringified version of the program.
	Version string
	// Copyright is the copyright string for the program (as some licenses
	// require something like "copyright (c) year name" somewhere in their
	// program)
	Copyright string
	// Info is a small description of the program.
	Info string
	// Usage is the command-line usage of the program. All occurrences of
	// "{name}" are replaced by Name. (Can be automatically set by using
	// Usage())
	Usage string
	// Args holds the arguments that will be parsed, and can be set/accessed by
	// their map name string.
	Args map[string]Arg
	// Extra holds any extra arguments that were not parsed with `-` or `--` or
	// were a value of one.
	Extra []string

	// Writer is the writer for where the output should be printed (for the
	// help menu). It will default to os.Stdout.
	Writer io.Writer
}

// Parse calls ParseSlice on os.Args[1:].
func (ap *ArgParser) Parse() bool {
	return ap.ParseSlice(os.Args[1:])
}

func (ap *ArgParser) assignArg(arg *Arg, idx int, args []string) (bool, bool) {
	skipNext := true
	switch v := arg.Value.(type) {
	case bool:
		arg.Value = !v
		skipNext = false
	case int:
		// len returns total_idx+1. Need one more additionally.
		if len(args) < idx+2 {
			return skipNext, false
		}
		a, err := strconv.Atoi(args[idx+1])
		if err != nil {
			return skipNext, false
		}
		arg.Value = a
	case string:
		if len(args) < idx+2 {
			return skipNext, false
		}
		arg.Value = args[idx+1]
	}
	return skipNext, true
}

// ParseSlice parses `args`, returning whether or not the parsing had errors.
func (ap *ArgParser) ParseSlice(args []string) bool {
	skipNext := false
	for idx, stringarg := range args {
		if skipNext {
			skipNext = false
			continue
		}
		ok := false
		if strings.HasPrefix(stringarg, "--") {
			stringarg = stringarg[2:]
			if len(stringarg) == 0 {
				ap.Extra = append(ap.Extra, args[idx+1:]...)
				return true
			}
			for name, arg := range ap.Args {
				if arg.Long != "" && arg.Long == stringarg {
					var bad bool
					skipNext, bad = ap.assignArg(&arg, idx, args)
					if !bad {
						return false
					}
					ap.Args[name] = arg
					ok = true
				}
			}
		} else if strings.HasPrefix(stringarg, "-") {
			for _, char := range stringarg[1:] {
				for name, arg := range ap.Args {
					if arg.Short != 0 && arg.Short == char {
						var bad bool
						skipNext, bad = ap.assignArg(&arg, idx, args)
						if !bad {
							return false
						}
						ap.Args[name] = arg
						ok = true
					}
				}
			}
		} else {
			ap.Extra = append(ap.Extra, stringarg)
			ok = true
		}
		if !ok {
			return false
		}
	}
	return true
}

// PrintHelp prints a pre-formatted help message for the program. Because the
// arguments are stored as a map, their order may be different between runs of
// the program, since maps are unordered.
func (ap ArgParser) PrintHelp() {
	if ap.Writer == nil {
		ap.Writer = os.Stdout
	}
	flags := make([]Arg, 0)
	options := make([]Arg, 0)
	for _, arg := range ap.Args {
		switch arg.Value.(type) {
		case bool:
			flags = append(flags, arg)
		default:
			options = append(options, arg)
		}
	}

	print := func(arg Arg) {
		var long, short string
		if arg.Short != 0 {
			short = "-" + string(arg.Short)
			if arg.Long != "" {
				short += ", "
			}
		}
		if arg.Long != "" {
			long = "--" + arg.Long
		}
		fmt.Fprintf(ap.Writer, "\t%s%s:\n\t\t%s\n", short, long, arg.Help)
	}

	fmt.Fprintf(ap.Writer, "%s %s\n%s\n%s\n%s\nUsage:\n\t%s\n",
		ap.Name,
		ap.Version,
		ap.Author,
		ap.Info,
		ap.Copyright,
		strings.ReplaceAll(ap.Usage, "{name}", ap.Name),
	)

	fmt.Fprintln(ap.Writer, "Flags:")
	for _, arg := range flags {
		print(arg)
	}
	fmt.Fprintln(ap.Writer, "Options:")
	for _, arg := range options {
		print(arg)
	}
}

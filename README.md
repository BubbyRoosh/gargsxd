# gargsxd

Similar to [rargsxd](https://codeberg.org/BubbyRoosh/rargsxd)!

Alternative argument parsing to the flag library.

## Benefits

The main benefit for most people is the better help dialog:

![help](help.png)

## Example

Example usage can be seen in [morg](https://codeberg.org/BubbyRoosh/morg)
